package synergy.challenge.movielist.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import synergy.challenge.movielist.model.response.MoviePopularResponse

interface ApiService {

    @GET("movie/popular")
    fun getMoviePopular(
        @Header("Authorization") apiKey : String = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkNGZhNTc2NDJkYWQ0NmE0Y2E2YmJkZWFjMTljYzQ3YiIsInN1YiI6IjY2NDYyODdmMjQzZjJmZDQxOTNhZmZjYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.FYaMemKuLDlt2wZ2B06-tc_RXpEh1o3lzOSEh5ujqG4"
    ) : Call<MoviePopularResponse>

}