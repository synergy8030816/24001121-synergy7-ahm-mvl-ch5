package synergy.challenge.movielist.view

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import synergy.challenge.movielist.R
import synergy.challenge.movielist.adapter.MovieListAdapter
import synergy.challenge.movielist.api.ApiClient
import synergy.challenge.movielist.databinding.FragmentHomePageBinding
import synergy.challenge.movielist.helper.Status
import synergy.challenge.movielist.model.response.MoviePopularResponse
import synergy.challenge.movielist.model.response.Result
import synergy.challenge.movielist.viewmodel.MovieViewModel
import synergy.challenge.movielist.viewmodel.MovieViewModelFactory


class HomePageFragment : Fragment(), MovieListAdapter.Companion.OnAdapterListener {
    private var _binding: FragmentHomePageBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(requireContext())
    }

    val callback:(Result) -> Unit = {}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomePageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getLogin().observe(viewLifecycleOwner){
            Log.e("CHANGELOGIN", "data login on home page = $it")
        }



        setupRecycleview()
        binding.toDetailProfil.setOnClickListener {
            findNavController().navigate(R.id.userProfilPageFragment)
        }


    }

    @SuppressLint("SuspiciousIndentation")
    private fun setupRecycleview() {
        val adapter = MovieListAdapter(callback, this)
                ApiClient.instance.getMoviePopular().enqueue(object :
                    Callback<MoviePopularResponse> {
            override fun onResponse(
                call: Call<MoviePopularResponse>,
                response: Response<MoviePopularResponse>
            ) {
                Log.e("SimpleDataAPI", Gson().toJson(response.body()))

                response.body()?.results?.toMutableList()?.let {
                    adapter.submitData(it)
                    binding.rvlistBucketMovie.adapter = adapter
                    binding.rvlistBucketMovie.layoutManager = LinearLayoutManager(activity)
                }

            }

            override fun onFailure(call: Call<MoviePopularResponse>, t: Throwable) {
                Log.e("SimpleDataAPI",Gson().toJson(t.message) )
            }

        })
    }

    override fun onClick(movieDetail: Result) {
        val bundle = Bundle()
        bundle.putParcelable(KEY_BUNDLE_MOVIE, movieDetail)
        findNavController().navigate(R.id.detailMovieFragment, bundle)
    }

    companion object{
        val KEY_BUNDLE_MOVIE = "KEY_BUNDLE_MOVIE"
    }
}