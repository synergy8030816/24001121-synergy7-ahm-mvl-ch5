package synergy.challenge.movielist.helper

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import synergy.challenge.movielist.model.DataUser

private val Context.dataStore : DataStore<Preferences> by preferencesDataStore(name = "user")

class MyDataStore(val context: Context) {
    val USER_KEY = stringPreferencesKey("counter_key")
    val LOGIN_KEY = booleanPreferencesKey("login_key")
    val USERNAME_USER_KEY = stringPreferencesKey("username_user_key")
    val PASSWORD_USER_KEY = stringPreferencesKey("password_user_key")
    val FULLNAME_USER_KEY = stringPreferencesKey("fullname_user_key")
    val BIRTHDATE_USER_KEY = stringPreferencesKey("birthdate_user_key")
    val ADDRESS_USER_KEY = stringPreferencesKey("address_user_key")
    val EMAIL_USER_KEY = stringPreferencesKey("email_user_key")


    suspend fun save(user: DataUser<String>){
        context.dataStore.edit {preferences ->
            preferences[USERNAME_USER_KEY] = user.username.toString()
            preferences[FULLNAME_USER_KEY] = user.fullName.toString()
            preferences[BIRTHDATE_USER_KEY] = user.birthDate.toString()
            preferences[ADDRESS_USER_KEY] = user.address.toString()
            preferences[PASSWORD_USER_KEY] = user.password.toString()
            preferences[EMAIL_USER_KEY] = user.email.toString()
        }
    }

    suspend fun setLogin(login: Boolean){
        context.dataStore.edit {preferences ->
            preferences[LOGIN_KEY] = login
        }
    }

    fun getLogin(): Flow<Boolean> {
        return context.dataStore.data.map {preferences ->
            preferences[LOGIN_KEY] ?: false
        }
    }

    fun getUsernameUser(): Flow<String>{
        return context.dataStore.data.map {preferences ->
            preferences[USERNAME_USER_KEY] ?: ""
        }
    }

    fun getFullnameUser(): Flow<String>{
        return  context.dataStore.data.map { preferences ->
            preferences[FULLNAME_USER_KEY] ?:  ""
        }
    }

    fun getBirthdateUser(): Flow<String>{
        return  context.dataStore.data.map { preferences ->
            preferences[BIRTHDATE_USER_KEY] ?: ""
        }
    }

    fun getAddressUser(): Flow<String>{
        return context.dataStore.data.map { preferences ->
            preferences[ADDRESS_USER_KEY] ?: ""
        }
    }

    fun getPasswordUser(): Flow<String>{
        return context.dataStore.data.map { preferences ->
            preferences[PASSWORD_USER_KEY] ?: ""
        }
    }

    fun getEmailUser(): Flow<String>{
        return context.dataStore.data.map { preferences ->
            preferences[EMAIL_USER_KEY] ?: ""
        }
    }



}