package synergy.challenge.movielist.view

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import synergy.challenge.movielist.R
import synergy.challenge.movielist.databinding.FragmentDetailMovieBinding
import synergy.challenge.movielist.databinding.FragmentHomePageBinding
import synergy.challenge.movielist.model.response.Result


class DetailMovieFragment : Fragment() {
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMovieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupMovieDetail()

    }

    private fun setupMovieDetail() {
        val movieDetail = arguments?.getParcelable<Result>(HomePageFragment.KEY_BUNDLE_MOVIE)
        if (movieDetail != null) {
            binding.titleMovie.text = movieDetail.originalTitle.toString()
            binding.overviewMovie.text = movieDetail.overview.toString()
            binding.voteMovie.text = movieDetail.voteAverage.toString()
            binding.popularity.text = movieDetail.popularity.toString()
            binding.releaseDate.text = movieDetail.releaseDate.toString()
            Glide.with(this)
                .asDrawable()
                .load("https://image.tmdb.org/t/p/w500" + movieDetail.posterPath)
                .into(binding.imageMovie)
        }
        Log.e("BUNDLEMOVIE", movieDetail.toString() )
    }

}