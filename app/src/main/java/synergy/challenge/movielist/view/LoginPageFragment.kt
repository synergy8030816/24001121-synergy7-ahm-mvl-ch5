package synergy.challenge.movielist.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import synergy.challenge.movielist.R
import synergy.challenge.movielist.databinding.FragmentLoginPageBinding
import synergy.challenge.movielist.databinding.FragmentUserProfilPageBinding
import synergy.challenge.movielist.viewmodel.MovieViewModel
import synergy.challenge.movielist.viewmodel.MovieViewModelFactory


class LoginPageFragment : Fragment() {
    private var _binding: FragmentLoginPageBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(requireContext())
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginPageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        viewModel.getLogin().observe(viewLifecycleOwner){islogin ->
            viewModel.getLogin().observe(viewLifecycleOwner){
                Log.e("CHANGELOGIN", "data login on login page = $it")
            }

            when(islogin){
                true -> {
                    toHomePage()
                }
                false ->{
                   loginSetup()
                }
            }


        }

        binding.navigateToRegister.setOnClickListener {
            findNavController().navigate(R.id.registerPageFragment)
        }
    }

    private fun loginSetup() {
        binding.textLogin.setOnClickListener {
            viewModel.getUsernameUserLiveData().observe(viewLifecycleOwner){
                if (it == binding.username.text.toString()){
                    viewModel.setLogin(true)
                    findNavController().navigate(R.id.homePageFragment)
                }
            }
        }
    }

    private fun toHomePage() {
        findNavController().navigate(R.id.homePageFragment)
    }


}