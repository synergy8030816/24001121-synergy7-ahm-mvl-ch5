package synergy.challenge.movielist.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import synergy.challenge.movielist.databinding.MovieListBucketBinding
import synergy.challenge.movielist.model.response.Result

class MovieListAdapter(
    private val callback: (Result) -> Unit,
    private val listener : OnAdapterListener
): RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {



    private val diffCallback = object : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem

        }
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)



    fun submitData(value: MutableList<Result?>) = differ.submitList(value)
    class ViewHolder(val binding: MovieListBucketBinding):  RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Result, callback: (Result) -> Unit, listener : OnAdapterListener){
            binding.titleMovie.text = data.originalTitle
            binding.overviewMovie.text = data.overview
            Glide.with(itemView)
                .asDrawable()
                .load("https://image.tmdb.org/t/p/w500" + data.posterPath)
                .into(binding.posterMovie)
            binding.root.setOnClickListener {
                listener.onClick(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = MovieListBucketBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(differ.currentList[position], callback, listener )
    }



    companion object{
        interface OnAdapterListener{
            fun onClick(movieDetail : Result)
        }
    }
}