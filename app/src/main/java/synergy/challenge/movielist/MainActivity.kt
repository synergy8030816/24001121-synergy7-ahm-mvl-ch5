package synergy.challenge.movielist.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.navigation.findNavController
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import synergy.challenge.movielist.R
import synergy.challenge.movielist.api.ApiClient
import synergy.challenge.movielist.databinding.ActivityMainBinding
import synergy.challenge.movielist.databinding.FragmentUserProfilPageBinding
import synergy.challenge.movielist.helper.Status
import synergy.challenge.movielist.model.response.MoviePopularResponse
import synergy.challenge.movielist.viewmodel.MovieViewModel
import synergy.challenge.movielist.viewmodel.MovieViewModelFactory

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}