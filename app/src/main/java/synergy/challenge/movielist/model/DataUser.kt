package synergy.challenge.movielist.model

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate

class DataUser<T> (
    val username: T?,
    val fullName : T?,
    val birthDate: T?,
    val address: T?,
    val password: T?,
    val email: T?
    )