package synergy.challenge.movielist.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import synergy.challenge.movielist.api.ApiClient
import synergy.challenge.movielist.api.RemoteDataSource
import synergy.challenge.movielist.helper.MyDataStore
import synergy.challenge.movielist.repository.MovieRepository

class MovieViewModelFactory(val remoteDataSource: RemoteDataSource, val pref : MyDataStore): ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: MovieViewModelFactory? = null

        fun getInstance(context: Context): MovieViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: MovieViewModelFactory(
                    RemoteDataSource(ApiClient.instance),
                    MyDataStore(context)
                )
            }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieViewModel::class.java)) {
            return MovieViewModel(MovieRepository(remoteDataSource), pref) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}