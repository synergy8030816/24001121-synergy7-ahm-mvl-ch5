package synergy.challenge.movielist.viewmodel

import androidx.datastore.preferences.protobuf.Api
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import synergy.challenge.movielist.api.ApiClient
import synergy.challenge.movielist.helper.MyDataStore
import synergy.challenge.movielist.helper.Resource
import synergy.challenge.movielist.model.DataUser
import synergy.challenge.movielist.repository.MovieRepository
import synergy.challenge.movielist.model.response.MoviePopularResponse
import javax.security.auth.callback.Callback

class MovieViewModel(private val repository: MovieRepository, private var pref :MyDataStore) : ViewModel() {

    private var _movieResponse = MutableLiveData<MoviePopularResponse?>()
    val movieResponse: LiveData<MoviePopularResponse?> get() = _movieResponse


    fun getMoviePopular() = liveData(Dispatchers.IO){
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = repository.searchAllPopularMovie()))
        } catch (exception: Exception){
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun saveUserLiveData(user: DataUser<String>){
        viewModelScope.launch {
            pref.save(user)
        }
    }

    fun setLogin(isLogin: Boolean){
        viewModelScope.launch {
            pref.setLogin(isLogin)
        }
    }

    fun getLogin() = pref.getLogin().asLiveData()

    fun getUsernameUserLiveData() = pref.getUsernameUser().asLiveData()
    fun getFullnameUserLiveData() = pref.getFullnameUser().asLiveData()
    fun getBirthdateUserLiveData() = pref.getBirthdateUser().asLiveData()

    fun getAddressUserLiveData() = pref.getAddressUser().asLiveData()
    fun getPasswordUserLiveData() = pref.getPasswordUser().asLiveData()
    fun getEmailUserLiveData() = pref.getEmailUser().asLiveData()




}
