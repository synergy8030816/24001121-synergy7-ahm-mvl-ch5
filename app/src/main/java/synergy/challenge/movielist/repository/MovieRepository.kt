package synergy.challenge.movielist.repository

import synergy.challenge.movielist.api.ApiService
import synergy.challenge.movielist.api.RemoteDataSource

class MovieRepository(private val remoteDataSource: RemoteDataSource) {

    suspend fun searchAllPopularMovie() = remoteDataSource.searchAllPopularMovie()
}