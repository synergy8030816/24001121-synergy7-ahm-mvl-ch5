package synergy.challenge.movielist.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import synergy.challenge.movielist.R
import synergy.challenge.movielist.databinding.FragmentLoginPageBinding
import synergy.challenge.movielist.databinding.FragmentRegisterPageBinding
import synergy.challenge.movielist.model.DataUser
import synergy.challenge.movielist.viewmodel.MovieViewModel
import synergy.challenge.movielist.viewmodel.MovieViewModelFactory

class RegisterPageFragment : Fragment() {
    private var _binding: FragmentRegisterPageBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(requireContext())
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterPageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRegisterUser()
    }

    private fun setupRegisterUser() {
        binding.textRegister.setOnClickListener {
            val user = DataUser(
                username = binding.inputUsername.text.toString(),
                password = binding.inputPassword.text.toString(),
                fullName = "",
                birthDate = "",
                address = "",
                email = binding.inputEmail.text.toString()
            )
            viewModel.saveUserLiveData(user)
            viewModel.getUsernameUserLiveData().observe(viewLifecycleOwner){
                Log.e("DATAUSERREGISTER", "username =  ${it}")
            }
            viewModel.getPasswordUserLiveData().observe(viewLifecycleOwner){
                Log.e("DATAUSERREGISTER", "password = ${it}")
            }
            viewModel.getEmailUserLiveData().observe(viewLifecycleOwner){
                Log.e("DATAUSERREGISTER", "email = ${it}")
            }
            findNavController().navigate(R.id.loginPageFragment)
        }
    }


}