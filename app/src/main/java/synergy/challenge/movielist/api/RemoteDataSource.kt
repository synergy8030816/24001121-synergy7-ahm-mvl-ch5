package synergy.challenge.movielist.api

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun searchAllPopularMovie() = apiService.getMoviePopular()

}