package synergy.challenge.movielist.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import synergy.challenge.movielist.R
import synergy.challenge.movielist.databinding.FragmentUserProfilPageBinding
import synergy.challenge.movielist.model.DataUser
import synergy.challenge.movielist.viewmodel.MovieViewModel
import synergy.challenge.movielist.viewmodel.MovieViewModelFactory

class UserProfilPageFragment : Fragment() {

    private var _binding: FragmentUserProfilPageBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(requireContext())
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserProfilPageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initProfileUser()
        updateUser()

    }

    private fun initProfileUser() {
    }

    private fun updateUser() {

        binding.btnLogout.setOnClickListener {
            viewModel.setLogin(false)
            findNavController().navigate(R.id.loginPageFragment)
        }

        binding.btnUpdate.setOnClickListener {

            viewModel.getPasswordUserLiveData().observe(viewLifecycleOwner){password ->
                viewModel.getEmailUserLiveData().observe(viewLifecycleOwner){email ->
                    val user = DataUser(
                        username = binding.etUsername.text.toString(),
                        fullName = binding.etFullname.text.toString(),
                        birthDate = binding.etBirthdate.text.toString(),
                        address =  binding.etAddress.text.toString(),
                        password = password,
                        email = email
                    )
                    viewModel.saveUserLiveData(user)
                    Toast.makeText(requireContext(), "update profile berhasil", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}